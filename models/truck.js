const mongoose = require('mongoose');

const TruckSchema = new mongoose.Schema({
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
    required: true,
  },
  status: {
    type: String,
    enum: ['IS', 'OL'],
    required: true,
    default: 'IS',
  },
  created_date: {
    type: Date,
    required: true,
    default: Date.now,
  },
  specs: {
    type: Map,
    of: Number,
  },
});
const trucksSpecs = {
  'SPRINTER': {
    width: 170,
    length: 300,
    height: 250,
    tonnage: 1700,
  },
  'SMALL STRAIGHT': {
    width: 170,
    length: 500,
    height: 250,
    tonnage: 2500,
  },
  'LARGE STRAIGHT': {
    width: 200,
    length: 700,
    height: 350,
    tonnage: 4000,
  },
};

TruckSchema.pre('save', function(next) {
  this.specs = trucksSpecs[this.type];
  next();
});

const Truck = mongoose.model('Truck', TruckSchema);

module.exports = Truck;
