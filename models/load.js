const mongoose = require('mongoose');

const states = [
  'En route to Pick Up',
  'Arrived to Pick Up',
  'En route to delivery',
  'Arrived to delivery',
];

const LoadSchema = new mongoose.Schema({
  created_by: {
    type: String,
  },
  assigned_to: {
    type: String,
    default: '',
  },
  name: {
    type: String,
  },
  state: {
    type: String,
    set: function(v) {
      if (!v) {
        return states[0];
      }
      const stateIdx = states.findIndex((state) => state === v);
      if (stateIdx > -1) {
        return states[stateIdx + 1] || states[states.length - 1];
      } else {
        throw new Error('Wrong state');
      }
    },
    enum: states,
  },
  payload: {
    type: Number,
  },
  status: {
    type: String,
    enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
    default: 'NEW',
  },
  pickup_address: {
    type: String,
  },
  delivery_address: {
    type: String,
  },
  dimensions: {
    type: Map,
    of: Number,
  },
  logs: [{message: String, time: {type: Date, default: Date.now}}],
  created_date: {
    type: Date,
    required: true,
    default: Date.now,
  },
});

// {
//     "_id": "5099803df3f4948bd2f98391",
//     "created_by": "5099803df3f4948bd2f98391",
//     "assigned_to": "5099803df3f4948bd2f98391",
//     "status": "NEW",
//     "state": "En route to Pick Up",
//     "name": "Moving sofa",
//     "payload": 100,
//     "pickup_address": "Flat 25, 12/F, Acacia Building 150 Kennedy Road",
//     "delivery_address": "Sr. Rodrigo Domínguez Av. Bellavista N° 185",
//     "dimensions": {
//       "width": 44,
//       "length": 32,
//       "height": 66
//     },
//     "logs": [
//       {
//         "message": "Load assigned to driver with id ###",
//         "time": "2020-10-28T08:03:19.814Z"
//       }
//     ],
//     "created_date": "2020-10-28T08:03:19.814Z"
//   }

// "name": "Moving sofa",
//   "payload": 100,
//   "pickup_address": "Flat 25, 12/F, Acacia Building 150 Kennedy Road",
//   "delivery_address": "Sr. Rodrigo Domínguez Av. Bellavista N° 185",
//   "dimensions": {
//     "width": 44,
//     "length": 32,
//     "height": 66
//   }

const Load = mongoose.model('Load', LoadSchema);

module.exports = Load;
