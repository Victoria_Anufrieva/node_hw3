const {Router: router} = require('express');
const {register, login, forgotPassword} = require('../controllers/auth');
const authRouter = router();

authRouter.post('/register', register);
authRouter.post('/login', login);
authRouter.post('/forgotPassword', forgotPassword);

module.exports = authRouter;
