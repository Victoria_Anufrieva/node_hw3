const {Router: router} = require('express');
const {
  create,
  getOne,
  getAll,
  update,
  del,
  post,
  getOneActiveDetailed,
  getOneActive,
  changeState,
} = require('../controllers/load');
const loadRouter = router();

loadRouter.get('/active', getOneActive);
loadRouter.post('/', create);
loadRouter.get('/', getAll);
loadRouter.get('/:id', getOne);
loadRouter.put('/:id', update);
loadRouter.delete('/:id', del);
loadRouter.post('/:id/post', post);
loadRouter.get('/:id/shipping_info', getOneActiveDetailed);
loadRouter.get('/active/state', changeState);

module.exports = loadRouter;
