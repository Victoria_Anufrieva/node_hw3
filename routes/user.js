const {Router: router} = require('express');
const {getOne, changePassword, del} = require('../controllers/user');
const userRouter = router();

userRouter.get('/me', getOne);
userRouter.patch('/me/password', changePassword);
userRouter.delete('/me', del);

module.exports = userRouter;
