const User = require('../models/user');
const bcrypt = require('bcrypt');

exports.getOne = async function(req, res, next) {
  try {
    const user = await User.findOne({_id: req.userId});
    if (!user) {
      return res.status(400).send({message: 'Not found'});
    }
    res.send({
      user: {
        email: user.email,
        role: user.role,
        _id: user._id,
        created_date: user.created_date,
      },
    });
  } catch (error) {
    res.status(500).send(error);
  }
};

exports.changePassword = async function(req, res, next) {
  const {oldPassword, newPassword} = req.body;
  if (!oldPassword || !newPassword) {
    return res.status(400).send({message: 'Password is missing'});
  }
  try {
    const user = await User.findOne({_id: req.userId});
    if (!user) {
      return res.status(400).send({message: 'User not found'});
    }
    const isMatch = await bcrypt.compare(oldPassword, user.password);
    if (!isMatch) {
      return res.status(400).send({message: 'Incorrect oldPassword'});
    }
    const hash = await bcrypt.hash(newPassword, 1);
    await User.updateOne({_id: req.userId}, {password: hash});
    res.send({message: 'Password changed successfully'});
  } catch (error) {
    res.status(500).send(error);
  }
};

exports.del = async function(req, res, next) {
  try {
    const deleteInfo = await User.deleteOne({_id: req.userId});
    if (deleteInfo.deletedCount < 1) {
      return res.status(400).send({message: 'Not found'});
    }
    res.send({message: 'Profile deleted successfully'});
  } catch (error) {
    res.status(500).send(error);
  }
};
