const Load = require('../models/load');
const Truck = require('../models/truck');

exports.getAll = async function(req, res, next) {
  if (req.userRole !== 'SHIPPER') {
    return res.status(400).send({message: 'Forbidden'});
  }
  const loads = await Load.find({created_by: req.userId});
  try {
    res.send({
      loads,
    });
  } catch (error) {
    res.status(500).send(error);
  }
};
exports.getOne = async function(req, res, next) {
  if (req.userRole !== 'SHIPPER') {
    return res.status(400).send({message: 'Forbidden'});
  }
  const {id} = req.params;
  try {
    const load = await Load.findOne({_id: id, created_by: req.userId});
    if (!load) {
      return res.status(400).send({message: 'Not found'});
    }
    res.send({load});
  } catch (error) {
    res.status(500).send(error);
  }
};
exports.getOneActive = async function(req, res, next) {
  if (req.userRole !== 'DRIVER') {
    return res.status(400).send({message: 'Forbidden'});
  }
  try {
    const load = await Load.findOne({assigned_to: req.userId});
    if (!load) {
      return res.status(400).send({message: 'Not found'});
    }
    res.send({load});
  } catch (error) {
    res.status(500).send(error);
  }
};
exports.getOneActiveDetailed = async function(req, res, next) {
  if (req.userRole !== 'SHIPPER') {
    return res.status(400).send({message: 'Forbidden'});
  }
  const {id} = req.params;
  try {
    const load = await Load.findOne({_id: id, assigned_to: {$ne: ''}});
    if (!load) {
      return res.status(400).send({message: 'Not found'});
    }
    const truck = await Truck.findOne({assigned_to: load.assigned_to});

    res.send({load, truck});
  } catch (error) {
    res.status(500).send(error);
  }
};
exports.create = async function(req, res, next) {
  if (req.userRole !== 'SHIPPER') {
    return res.status(400).send({message: 'Forbidden'});
  }
  // if (!req.body.type) {
  //   return res.status(400).send({ message: 'Type is missing' });
  // }
  try {
    const newLoad = new Load({...req.body, created_by: req.userId});
    await newLoad.save();
    res.send({
      message: 'Load created successfully',
    });
  } catch (error) {
    res.status(500).send(error);
  }
};
exports.post = async function(req, res, next) {
  if (req.userRole !== 'SHIPPER') {
    return res.status(400).send({message: 'Forbidden'});
  }
  const {id} = req.params;
  try {
    const load = await Load.findOne({
      _id: id,
      created_by: req.userId,
      status: 'NEW',
    });
    if (!load) {
      return res.status(400).send({message: 'Not found'});
    }
    await Load.updateOne({_id: id}, {status: 'POSTED'});
    const trucks = await Truck.find({assigned_to: {$ne: ''}, status: 'IS'});
    const truck = trucks.find((truck) => {
      return (
        truck.specs.get('width') >= load.dimensions.get('width') &&
        truck.specs.get('height') >= load.dimensions.get('height') &&
        truck.specs.get('length') >= load.dimensions.get('length') &&
        truck.specs.get('tonnage') >= load.payload
      );
    });
    if (!truck) {
      const log = {
        message: 'Driver not found',
        time: new Date().toJSON(),
      };
      await Load.updateOne(
          {_id: id},
          {status: 'NEW', $push: {logs: log}},
      );
      return res
          .status(400)
          .send({message: 'Driver not found. Try again later'});
    }
    const log = {
      message: `Load assigned to driver with id ${truck.assigned_to}`,
      time: new Date().toJSON(),
    };
    await Load.updateOne(
        {_id: id},
        {
          assigned_to: truck.assigned_to,
          status: 'ASSIGNED',
          state: 'En route to Pick Up',
          $push: {logs: log},
        },
    );
    await Truck.updateOne({_id: truck._id}, {status: 'OL'});
    res.send({
      message: 'Load posted successfully',
      driver_found: true,
    });
  } catch (error) {
    res.status(500).send(error);
  }
};
exports.update = async function(req, res, next) {
  if (req.userRole !== 'SHIPPER') {
    return res.status(400).send({message: 'Forbidden'});
  }
  // if (!req.body.type) {
  //   return res.status(400).send({ message: 'Type is missing' });
  // }
  const {id} = req.params;
  try {
    const load = await Load.findOne({
      _id: id,
      created_by: req.userId,
      status: 'NEW',
    });
    if (!load) {
      return res.status(400).send({message: 'Not found'});
    }
    await Load.updateOne({_id: id}, {...req.body});
    res.send({message: 'Success'});
  } catch (error) {
    res.status(500).send(error);
  }
};
exports.changeState = async function(req, res, next) {
  if (req.userRole !== 'DRIVER') {
    return res.status(400).send({message: 'Forbidden'});
  }
  try {
    const load = await Load.findOne({assigned_to: req.userId});
    if (!load) {
      return res.status(400).send({message: 'Not found'});
    }
    await Load.updateOne({_id: load._id}, {state: load.state});
    const updatedLoad = await Load.findOne({assigned_to: req.userId});
    const log = {
      message: `Load change state to ${updatedLoad.state}`,
      time: new Date().toJSON(),
    };
    await Load.updateOne({_id: load._id}, {$push: {logs: log}});
    res.send({message: `Load state changed to '${updatedLoad.state}'`});
  } catch (error) {
    res.status(500).send(error);
  }
};

exports.del = async function(req, res, next) {
  if (req.userRole !== 'SHIPPER') {
    return res.status(400).send({message: 'Forbidden'});
  }
  const {id} = req.params;
  try {
    const deleteInfo = await Load.deleteOne({
      _id: id,
      created_by: req.userId,
      status: 'NEW',
    });
    if (deleteInfo.deletedCount < 1) {
      return res.status(400).send({message: 'Not found'});
    }
    res.send({message: 'Load deleted successfully'});
  } catch (error) {
    res.status(500).send(error);
  }
};
