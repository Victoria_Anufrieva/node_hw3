const Truck = require('../models/truck');

exports.getAll = async function(req, res, next) {
  if (req.userRole !== 'DRIVER') {
    return res.status(400).send({message: 'Forbidden'});
  }
  const trucks = await Truck.find({created_by: req.userId});
  try {
    res.send({
      trucks,
    });
  } catch (error) {
    res.status(500).send(error);
  }
};
exports.getOne = async function(req, res, next) {
  const {id} = req.params;
  try {
    const truck = await Truck.findOne({_id: id, created_by: req.userId});
    if (!truck) {
      return res.status(400).send({message: 'Not found'});
    }
    res.send({truck});
  } catch (error) {
    res.status(500).send(error);
  }
};
exports.create = async function(req, res, next) {
  if (req.userRole !== 'DRIVER') {
    return res.status(400).send({message: 'Forbidden'});
  }
  if (!req.body.type) {
    return res.status(400).send({message: 'Type is missing'});
  }
  try {
    const newTruck = new Truck({
      type: req.body.type,
      created_by: req.userId,
      assigned_to: req.userId,
    });
    await newTruck.save();
    await Truck.updateMany({assigned_to: req.userId}, {assigned_to: ''});
    res.send({
      message: 'Truck created successfully',
    });
  } catch (error) {
    res.status(500).send(error);
  }
};
exports.assign = async function(req, res, next) {
  if (req.userRole !== 'DRIVER') {
    return res.status(400).send({message: 'Forbidden'});
  }
  const {id} = req.params;
  try {
    const truck = await Truck.findOne({_id: id, created_by: req.userId});
    if (!truck) {
      return res.status(400).send({message: 'Not found'});
    }
    await Truck.updateMany({assigned_to: req.userId}, {assigned_to: ''});
    await Truck.updateOne(
        {_id: id, created_by: req.userId},
        {assigned_to: req.userId},
    );
    res.send({message: 'Truck assigned successfully'});
  } catch (error) {
    res.status(500).send(error);
  }
};
exports.update = async function(req, res, next) {
  if (req.userRole !== 'DRIVER') {
    return res.status(400).send({message: 'Forbidden'});
  }
  if (!req.body.type) {
    return res.status(400).send({message: 'Type is missing'});
  }
  const {id} = req.params;
  try {
    const truck = await Truck.findOne({
      _id: id,
      created_by: req.userId,
      assigned_to: '',
    });
    if (!truck) {
      return res.status(400).send({message: 'Not found'});
    }
    await Truck.updateOne({_id: id}, {type: req.body.type});
    res.send({message: 'Success'});
  } catch (error) {
    res.status(500).send(error);
  }
};
exports.del = async function(req, res, next) {
  if (req.userRole !== 'DRIVER') {
    return res.status(400).send({message: 'Forbidden'});
  }
  const {id} = req.params;
  try {
    const deleteInfo = await Truck.deleteOne({
      _id: id,
      created_by: req.userId,
      assigned_to: '',
    });
    if (deleteInfo.deletedCount < 1) {
      return res.status(400).send({message: 'Not found'});
    }
    res.send({message: 'Truck deleted successfully'});
  } catch (error) {
    res.status(500).send(error);
  }
};
